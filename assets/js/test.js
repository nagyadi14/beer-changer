var number = 0;
var test = [
    {
        "question" : "x + x + x = 18<br>x + y + y = 14<br>y - z = 2<br>x + y + z = ?", 
        "answer"   : "12"
    },
    {
        "question" : "x + x + x = 30<br>x + y + y = 18<br>y - z = 2<br>x + y + z = ?", 
        "answer"   : "16"
    },
    {
        "question" : "x = 3<br>y = -3<br>z = 24<br>x + z / x - y = ?", 
        "answer"   : "14"
    },
    {
        "question" : "x = 7<br>y = 5 + x<br>x = 1 + z<br>x + y + z / 3 = ?", 
        "answer"   : "21"
    },
    {
        "question" : "x + y + x = 21<br>10 = z + 4 + z<br>z + v = 8<br>14 = x + v<br>x + y + z + v = ?", 
        "answer"   : "20"
    }
];

$("#btn-start").click(function() {
    $("#question").html(test[number].question);
    $("#answer, textarea").val("");
}); 

$("#btn-next").click(function() {     
    if (number < 4) {        
        if ( $("#answer").val() == test[number].answer ) {
            number++;
            $("#question").html(test[number].question);
            alert("Helyes válasz, ihat a következő! :)");
            $("#answer, textarea").val("");
        } else {
            alert("Nem talált, próbáld újra! :(");
            $("#answer, textarea").val("");
        }        
    } else if (number === 4) {        
        if ( $("#answer").val() == test[number].answer ) {
            $("#end").val("1");
            $("#btn-next, #question, #answer").hide();
            alert("VÉGE! Mutasd meg az időtöket a szervezőknek :)");
            $("#answer, textarea").val("");
        } else {
            alert("Nem talált, próbáld újra! :(");
            $("#answer, textarea").val("");
        }      
    }    
}); 