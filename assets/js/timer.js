$("#btn-start").click(function() {    
    //indításkor idő lekérdezése
    var startTime = new Date($.now());
    $("#start-time").html("Start: " + startTime.getHours() + " óra " + startTime.getMinutes() + " perc " + startTime.getSeconds() + " másodperc");
    
    //stopper    
    var timerMinutes = 0;
    var timerSeconds = 0;      
    var timer = setInterval(function() {       
        if ($("#end").val() == 0) {
            timerSeconds++;
            if (timerSeconds > 59) {
                timerSeconds = 0;
                timerMinutes++;
                if (timerMinutes > 59) {
                    timerMinutes = 0;                
                }
            }  
        }
        $("#timer").html("Stopper: " + timerMinutes + " perc " + timerSeconds + " másodperc");        
    }, 1000);  
}); 